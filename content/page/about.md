---
title: About
description: Who I am
menu: main
weight: -210

cover:
  image: /images/portrait.jpg
  caption: That's me
  style: normal
---

My name is Pierce Bartine. I'm a programmer working in Austin, Texas for Clear Measure. My goal is to become [full stack][1] in whatever currently has my attention. There are so many disparate bodies of knowledge out there that I can't help but want to [connect them][2]! Really, I just love learning, no matter the topic.

Outside of that, I'm also a swimmer (visualizing swimming data got me into programming). Water is my home; I like the beach, islands, and conoeing/kayaking, while skiing, camping, and caving are also pretty cool. Maybe after automating everything else, I'll have more time do this stuff! :ocean:

[1]: https://intenseminimalism.com/2015/where-are-the-polymaths-hiding/
[2]: https://en.wikipedia.org/wiki/History_of_the_Actor_model#Relationship_to_physics